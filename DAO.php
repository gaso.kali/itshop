<?php
require_once 'db.php';

class DAO {
	private $db;

	
	
	private $SELECT_USER_BY_USERNAME_AND_PASSWORD = "SELECT * FROM user join atributes on user.id = atributes.id where user.username = ? and user.password = ?";	
	private $SELECT_USER_BY_USERNAME = "SELECT * FROM user where username = ? ";	
	private $INSERT_USER_ATRIBUTES = "INSERT into atributes (ime, prezime, email) values (?,?,?) ";	
	private $INSERT_USER = "INSERT into user (type, username, password, atributes) values ('user',?,?,?) ";	
	
	public function __construct()
	{
		$this->db = DB::createInstance();
	}

	public function selectUserByUsernameAndPassword($username, $password)
	{
		
		$statement = $this->db->prepare($this->SELECT_USER_BY_USERNAME_AND_PASSWORD);
		$statement->bindValue(1, $username);
		$statement->bindValue(2, $password);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}

	public function selectUserByUsername($username)
	{
		
		$statement = $this->db->prepare($this->SELECT_USER_BY_USERNAME);
		$statement->bindValue(1, $username);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}

	private function insertUserAtributes($ime, $prezime, $email)
	{
		
		$statement = $this->db->prepare($this->INSERT_USER_ATRIBUTES);
		$statement->bindValue(1, $ime);
		$statement->bindValue(2, $prezime);
		$statement->bindValue(3, $email);
		
		$statement->execute();
		// last insert id
		return $this->db->lastInsertID();
	}
	private function insertUser($username, $password, $atributes)
	{
		
		$statement = $this->db->prepare($this->INSERT_USER);
		$statement->bindValue(1, $username);
		$statement->bindValue(2, $password);
		$statement->bindValue(3, $atributes);
		
		$statement->execute();
	}
	public function insertUserWithAtributes($ime, $prezime, $email, $username, $password)
	{
		try{
			$this->db->beginTransaction(); // poceti tranksakciju upita
			$atributes  = $this->insertUserAtributes($ime, $prezime, $email);
			$this->insertUser($username, $password, $atributes);
			$this->db->commit();			// ako je sve ok onda commit
			return true;
		}catch(PDOException $e){
			$this->db->rollback();			// vracanje na pocetno stanje u slucaju bilo koje greske u bilo kojem upitu
			return false;
		}
	}
}
?>
