<?php


require_once 'DAO.php';

$action = isset($_REQUEST["action"])? $_REQUEST["action"] : ""; 

// var_dump($action);
if ($_SERVER['REQUEST_METHOD']=="POST"){
    
    if ($action == 'LOGIN') {
        $username = isset($_POST["username"])? test_input($_POST["username"]) : ""; 
        $password = isset($_POST["password"])? test_input($_POST["password"]) : ""; 
        // validacije
        $dao = new DAO();
        $user = $dao->selectUserByUsernameAndPassword($username,$password);
        if($user){
            // $msg = "Uspesno logovanje!!!";

            // ubaciti usera u sesiju i prebaciti ga na svoju pocetnu stranu
            session_start();
            $_SESSION['user'] = $user;
            $_SESSION['last-active'] = time();
           // var_dump($user);
           /*
            switch($user['type']){
                case 'admin'; include_once 'admin.php'; break;
                case 'kupac'; include_once 'kupac.php'; break;
                case 'prodavac'; include_once 'prodavac.php'; break;
                default; $msg = 'pogresan tip'; include_once 'index.php'; break;
            }
            */
            // cookie remember me
            if (isset($_POST['rememberMe'])) {
                $userCookie = array('username' => $_POST['username'], 'password' => $_POST['password']);
            
                setcookie("userJSON", json_encode($userCookie), time() + 20, "/");
            }

            include_once $user['type'].'.php';
        }else{
            $msg = "Pogresni parametri za logovanje!!!";
            include_once 'index.php';
        }
        
    } elseif ($action == 'REGISTER') {
        // $podatak = isset($_POST["podatak"])? test_input($_POST["podatak"]) : ""; //provera da li su setovani podaci
        //...
        // validacije
   
        $ime = isset($_POST["ime"])? test_input($_POST["ime"]) : ""; 
        $prezime = isset($_POST["prezime"])? test_input($_POST["prezime"]) : ""; 
        $email = isset($_POST["email"])? test_input($_POST["email"]) : ""; 
        $username = isset($_POST["username"])? test_input($_POST["username"]) : ""; 
        $password = isset($_POST["password"])? test_input($_POST["password"]) : ""; 
        $repeatPassword = isset($_POST["repeatPassword"])? test_input($_POST["repeatPassword"]) : ""; 
        $robot = isset($_POST["robot"])? test_input($_POST["robot"]) : ""; 

        if($robot == "") header("location: register.php"); // zastita dal je robot, automatska redirekcija 

        if(!($ime == "" || $prezime == "" || $email == "" || $username == "" || $password == "" || $repeatPassword == "" )){
            if($password == $repeatPassword){ 
                $dao = new DAO();
                if(!$dao->selectUserByUsername($username)){ 
                    // insert u bazu

                    // private metode
                    // $id_user_atributes = $dao->insertUserAtributes($ime, $prezime, $email);
                    //$dao->insertUser($username,$password, $id_user_atributes);
                   
                    if($dao->insertUserWithAtributes($ime, $prezime, $email,$username, $password)==false){
                        $msg = "Doslo je do greske prilikom registracije. Pokusajte kasnije";
                    }else{
                        $msg = "Uspesno ste se registrovali. Sada se mozete ulogovati";
                    }
                    include_once 'register.php';

                }else{
                    $msg = "Username '$username' je zauzet";
                    include_once 'register.php';
                }
            }else{
                $msg = "Sifre se moraju poklapati";
                include_once 'register.php';
            }
        }else{
            $msg = "Morate popuniti sva polja";
            include_once 'register.php';
        }




    } 
    
} elseif ($_SERVER['REQUEST_METHOD']=="GET"){
    //akcije za prikaz i brisanje
    if ($action == 'logout') {
        session_start();
        session_unset();
        session_destroy();
    
        header('Location: index.php');
    } elseif ($action == 'akcijaGet2'){
        //...
    }elseif ($action == 'akcijaGet3'){
        //...
    }
    
} else {
    //...
    header("Location: index.php"); //opciono
    die();
}

//funkcija za preradu unetih podataka
function test_input($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>