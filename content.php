<?php include_once 'navbar.php'?>

<body>
    
<div id="products"></div>
</body>
<?php include_once 'footer.php'?>


</html>
<?php

$productsList = [
    [
        'name' => 'Samsung 123',
        'type' => 'mobile',
        'active' => false,
    ],
    [
        'name' => 'Iphone 21',
        'type' => 'web',
        'active' => true,
    ],
    [
        'name' => 'Huawei',
        'type' => 'web',
        'active' => true,
    ],
    [
        'name' => 'Honor',
        'type' => 'web',
        'active' => 'false',
    ],
];

// ?>

<script>


let productsList = <?php echo json_encode($productsList); ?>

var productsHtml = '';

for (let index = 0; index < productsList.length; index++) {
    if (productsList[index].active) {

        productsHtml += productsList[index].name + ' </br>';
    }
}

document.getElementById('products-list').innerHTML = productsHtml;

</script>
