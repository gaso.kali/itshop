//Validation of files input
function ValidateExtension() {
    var allowedFiles = [".doc", ".docx", ".pdf"];
    var fileUpload = document.getElementById("button1");
    var lblError = document.getElementById("lblError");
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
    if (!regex.test(fileUpload.value.toLowerCase())) {
        lblError.innerHTML = "Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.";
        return false;
    }
    lblError.innerHTML = "";
    return true;
}


//Validation of email input
function validanMejl(obj) {
    obj.value = obj.value.trim();
    if (obj.value.match(/^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i) === null) {
        obj.style.color = 'red';
        return false;
    }
    obj.style.color = 'green';
    return true;
}
function validacijaForme() {
    var emailInput = document.getElementById('email');
    var validan = validanMejl(emailInput);
    if (!validan)
        emailInput.focus();
    return validan;
}

